﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        int j;
        int k;
        Form2 f2;
        Form3 f3;
        public Form1()
        {
            InitializeComponent();
        }
        public Form1(object a)
        {
            if (a is Form2)
            {
                f2 = (Form2)a;
            }
            else if (a is Form3)
            {
                f3 = (Form3)a;
            }
            else
            {
                f2 = null;
                f3 = null;

                MessageBox.Show("Invalid");
            }
            InitializeComponent();
        }
        private void No_Click(object sender, EventArgs e)
        {

            this.Hide();
            Form2 f5 = new Form2(this);
            f5.Show();
        }
        private void Yes_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form3 f5 = new Form3();
            f5.Show();
        }
        private void No_MouseMove(object sender, MouseEventArgs e)
        {
            Random x = new Random();
           
            Point pt = new Point(
                int.Parse(x.Next(400).ToString()),
                int.Parse(x.Next(250).ToString())
                );
            No.Location = pt;
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            j = this.Height;
            k = this.Width;
        }

        private void Form1_ClientSizeChanged(object sender, EventArgs e)
        {
            if (f2==null||f3==null)
            {
                this.WindowState = FormWindowState.Normal;
            }
            else if (f2.Height > 621 && f2.Width > 549)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else if (f3.Height > 621 && f3.Width > 549)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                
            }
        }

        
    }
}
