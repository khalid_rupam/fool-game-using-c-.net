﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form2 : Form
    {
        Form1 f1;
        public Form2()
        {
            InitializeComponent();
        }
        public Form2(Form1 f)
        {
            f1 = f;
            InitializeComponent();
        }
        private void Replay_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 f1 = new Form1(this);
            f1.Show();
        }
        private void Close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form2_ClientSizeChanged(object sender, EventArgs e)
        {
            if (f1.Height > 621 && f1.Width > 549)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            
        }

        private void Replay_LocationChanged(object sender, EventArgs e)
        {
        }

        private void Replay_ClientSizeChanged(object sender, EventArgs e)
        {
            this.Anchor = f1.Anchor;
        }

        private void Close_AutoSizeChanged(object sender, EventArgs e)
        {
            this.Anchor = f1.Anchor;
        }
    }
}
